package testscripts;

import org.testng.annotations.Test;

import base.DemoTestBase;
import configuration.annotations.ApplicationTypes;
import configuration.annotations.DataProviderParameters;
import configuration.applications.utils.ApplicationType;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import testDataHandler.TestData;

public class AddProductTest extends DemoTestBase{

	
	@Test(dataProvider="TestDataProvider", groups = { "regression" })
	@DataProviderParameters(path="LoginData.json")
	@ApplicationTypes(types={ApplicationType.MobileApplication})
	public void AddProductInJob(TestData data) {
				
		LoginPage login = mobileApplication.newPage(LoginPage.class);
		login.clickOnLogo();
		login.loginPopUp(testData.get("AdminUserName"), testData.get("AdminPassword"));		
		login.enterUrl(testData.get("DevUrl"), testData.get("DevApi"));		
		login.login(testData.get("UserName"), testData.get("Password"));
		
		HomePage home = mobileApplication.newPage(HomePage.class);
		home.clickJobInBlendedTicket();
	}
}