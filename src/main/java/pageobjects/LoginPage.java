package pageobjects;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import configuration.base.PageBase;
import configuration.driverManager.DriverManager;
import helpers.WaitType;


public class LoginPage extends PageBase {
	Properties obj = new Properties();

	{
		FileInputStream objfile = null;
		try {
			if (testEnvironment.getProperty("mobileDriverType").toLowerCase().equals("android")) {

				objfile = new FileInputStream(
						System.getProperty("user.dir") + "\\ObjectRepo\\Android\\LoginPage.properties");

			} else if (testEnvironment.getProperty("mobileDriverType").toLowerCase().equals("ios")) {
				objfile = new FileInputStream(
						System.getProperty("user.dir") + "\\ObjectRepo\\iOS\\LoginPage.properties");
			}

			obj.load(objfile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private By _Logo = By.id(obj.getProperty("Logo"));
	private By _Email = By.xpath(obj.getProperty("Email"));
	private By _Pwd = By.xpath(obj.getProperty("Pwd"));
	private By _okButton = By.id(obj.getProperty("OkButton"));
	private By _signIn = By.id(obj.getProperty("SignIn"));
	private By _SignInpwd = By.id(obj.getProperty("SignInPwd"));
	private By _LoginBtn = By.id(obj.getProperty("LoginBtn"));

	private By _dentity_Url = By.id(obj.getProperty("Dentity_Url"));
	private By _api_Url = By.id(obj.getProperty("Api_Url"));
	private By _submit = By.id(obj.getProperty("Submit"));
	
	

	public void clickOnLogo() {

		if(mobileVerifies.isAlertPresent()) {
			DriverManager.getMobileDriver().switchTo().alert().accept();
		}		

		waitForLogoToBeVisible();
		
		int click_no = 5;
		for (int i = 1; i <= click_no; i++) {
			mobileActions.click(_Logo);
		}
	}
	
	public void waitForLogoToBeVisible() {
		mobileWaits.waitForVisibilityOfMobileElementLocated(_Logo, WaitType.Small);
	}

	public void loginPopUp(String username, String password) {
		mobileActions.sendKeys(_Email, username);
		mobileActions.sendKeys(_Pwd, password);
		mobileActions.click(_okButton);
	}

	public void login(String username, String password) {
		mobileActions.sendKeys(_signIn, username);
		mobileActions.sendKeys(_SignInpwd, password);
		mobileActions.hideKeyBoard();
		mobileActions.click(_LoginBtn);
		
		if(mobileVerifies.isAlertPresent()) {
			DriverManager.getMobileDriver().switchTo().alert().accept();
		}	
	}

	public void enterUrl(String Url, String Api) {
		
		mobileWaits.waitForVisibilityOfMobileElementLocated(_dentity_Url, WaitType.Small);
		mobileActions.click(_dentity_Url);
		mobileActions.clear(_dentity_Url);
		mobileActions.sendKeys(_dentity_Url, Url);
		
		mobileActions.click(_api_Url);
		mobileActions.clear(_api_Url);		
		mobileActions.sendKeys(_api_Url, Api);
		mobileActions.click(_submit);
	}

}
