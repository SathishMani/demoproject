package pageobjects;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;

import configuration.base.PageBase;
import configuration.driverManager.DriverManager;
import helpers.WaitType;

public class SettingsPage extends PageBase {

	Properties obj = new Properties();

	{
		FileInputStream objfile = null;

		try {
			if (testEnvironment.getProperty("mobileDriverType").toLowerCase().equals("android")) {

				objfile = new FileInputStream(
						System.getProperty("user.dir") + "\\ObjectRepo\\Android\\SettingsPage.properties");

			} else if (testEnvironment.getProperty("mobileDriverType").toLowerCase().equals("ios")) {
				objfile = new FileInputStream(
						System.getProperty("user.dir") + "\\ObjectRepo\\iOS\\SettingsPage.properties");
			}

			obj.load(objfile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private By _settingsBtn = By.xpath(obj.getProperty("Settings"));
	private By _signOutBtn = By.id(obj.getProperty("SignOut"));

	public void clickSettings() {
		try {

			if (testEnvironment.getProperty("mobileDriverType").toLowerCase().equals("ios")) {
				mobileWaits.waitForAlertIsPresent(WaitType.Medium);
				DriverManager.getMobileDriver().switchTo().alert().accept();
				mobileWaits.waitForAlertIsPresent(WaitType.Medium);
				DriverManager.getMobileDriver().switchTo().alert().accept();
			}

			mobileWaits.waitForVisibilityOfMobileElementLocated(_settingsBtn, WaitType.Small);
			mobileActions.click(_settingsBtn);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void clickSignOut() {
		mobileWaits.waitForVisibilityOfMobileElementLocated(_signOutBtn, WaitType.Small);
		mobileActions.click(_signOutBtn);
	}

}
