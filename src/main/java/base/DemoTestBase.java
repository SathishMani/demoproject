package base;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

import configuration.base.TestBase;
import configuration.driverManager.DriverSettings;
import io.appium.java_client.remote.MobileCapabilityType;

public class DemoTestBase extends TestBase {

	@BeforeMethod(alwaysRun=true)
	public void beforeTest() throws MalformedURLException, IOException {
		
		DesiredCapabilities capabilities = new DesiredCapabilities();

		DriverSettings settings = new DriverSettings();

		File app = null;

		String mobileDriverType = testEnvironment.getProperty("mobileDriverType").toLowerCase();

		// For Android
		if (mobileDriverType.equals("android")) {
			
			app = new File(testEnvironment.getProperty("apkPath"));
			capabilities.setCapability(MobileCapabilityType.APP, app.getCanonicalPath());
			capabilities.setCapability("appPackage", testEnvironment.getProperty("appPackage"));

			settings.androidCapabilities = capabilities;
			settings.androidRemoteAddress = new URL(testEnvironment.getProperty("appiumServerUrl"));

		} 
		// For iOS
		else if (mobileDriverType.equals("ios")) { 
			
			app = new File(testEnvironment.getProperty("ipaPath"));
			capabilities.setCapability(MobileCapabilityType.APP, testEnvironment.getProperty("ipaPath"));
			capabilities.setCapability("udid", testEnvironment.getProperty("udid"));
			capabilities.setCapability("bundleId", testEnvironment.getProperty("bundleId"));
			//capabilities.setCapability("autoAcceptAlerts", testEnvironment.getProperty("autoAcceptAlerts"));
			//capabilities.setCapability("simpleIsVisibleCheck", false);
			capabilities.setCapability("locationServicesEnabled",
					Boolean.valueOf(testEnvironment.getProperty("LocationServicesEnabled")));
			capabilities.setCapability("locationServicesAuthorized",
					Boolean.valueOf(testEnvironment.getProperty("LocationServicesAuthorized")));

			settings.iosCapabilities = capabilities;
			settings.iosRemoteAddress = new URL(testEnvironment.getProperty("appiumServerUrl"));

		}

		capabilities.setCapability("deviceName", testEnvironment.getProperty("deviceName"));
		capabilities.setCapability("platformName", testEnvironment.getProperty("platformName"));
		capabilities.setCapability("platformVersion", testEnvironment.getProperty("platformVersion"));
		capabilities.setCapability("autoGrantPermissions",
				Boolean.valueOf(testEnvironment.getProperty("autoGrantPermissions")));

		settings.mobileDriverType = testEnvironment.getProperty("mobileDriverType");
		mobileApplication.launch(settings);
	}

	@AfterTest
	public void afterClass() {
	}
}
