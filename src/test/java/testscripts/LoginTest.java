package testscripts;

import org.testng.annotations.Test;

import base.DemoTestBase;
import configuration.annotations.ApplicationTypes;
import configuration.annotations.DataProviderParameters;
import configuration.applications.utils.ApplicationType;
import pageobjects.LoginPage;
import pageobjects.SettingsPage;
import testDataHandler.TestData;

public class LoginTest extends DemoTestBase{

	@Test(dataProvider="TestDataProvider", groups = { "smoke" })
	@DataProviderParameters(path="LoginData.json")
	@ApplicationTypes(types={ApplicationType.MobileApplication})
	public void LoginTestMethod(TestData data) {
				
		LoginPage login = mobileApplication.newPage(LoginPage.class);
		login.clickOnLogo();
		login.loginPopUp(testData.get("AdminUserName"), testData.get("AdminPassword"));		
		login.enterUrl(testData.get("DevUrl"), testData.get("DevApi"));		
		login.login(testData.get("UserName"), testData.get("Password"));
		
		SettingsPage settings = mobileApplication.newPage(SettingsPage.class);
		settings.clickSettings();
		settings.clickSignOut();
		
		login.waitForLogoToBeVisible();
	}

}