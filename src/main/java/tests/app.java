package tests;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

/**
 * Hello world!
 *
 */
public class app {
	public static void main(String[] args) {
		try {

			DesiredCapabilities capabilities = new DesiredCapabilities();

			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("platformVersion", "8.1.0");
			capabilities.setCapability("deviceName", "Nexus 6 API 27");
			capabilities.setCapability("automationName", "UiAutomator2");
			//capabilities.setCapability("appActivity", "md58818a8f0a423971701db0aca4e49e292.MainActivity");
			capabilities.setCapability("appPackage", "com.agvancesky.apply");
			capabilities.setCapability("app", "C:\\Users\\sathishkumarm\\AppData\\Local\\Android\\Sdk\\platform-tools\\com.agvancesky.apply.apk");

			AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(
					new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			driver.launchApp();

		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
}
