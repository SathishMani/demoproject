package pageobjects;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.By;

import configuration.base.PageBase;
import helpers.WaitType;

public class HomePage extends PageBase {

	Properties obj = new Properties();
	
	{
		FileInputStream objfile = null;
		
		try {
			if (testEnvironment.getProperty("mobileDriverType").toLowerCase().equals("android")) {

				objfile = new FileInputStream(
						System.getProperty("user.dir") + "\\ObjectRepo\\Android\\HomePage.properties");

			} else if (testEnvironment.getProperty("mobileDriverType").toLowerCase().equals("ios")) {
				objfile = new FileInputStream(
						System.getProperty("user.dir") + "\\ObjectRepo\\iOS\\HomePage.properties");
			}

			obj.load(objfile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private By _blendedTicket = By.xpath(obj.getProperty("BlendedTicket"));
	private By _products = By.xpath(obj.getProperty("Product"));
	private By _addProduct = By.id(obj.getProperty("AddProduct"));
	private By _productText = By.id(obj.getProperty("ProductText"));
	private By _addedProduct = By.xpath(obj.getProperty("AddedProduct"));
	private By _moveBackward = By.xpath(obj.getProperty("MoveBackward"));
	private By _conditions = By.xpath(obj.getProperty("Conditions"));
	private By _fieldCondition = By.xpath(obj.getProperty("FieldCondition"));
	private By _soilMoisture = By.xpath(obj.getProperty("SoilMoisture"));
	private By _soilTexture = By.xpath(obj.getProperty("SoilTexture"));

	public void clickJobInBlendedTicket() {
		
		mobileWaits.waitForVisibilityOfMobileElementLocated(_blendedTicket, WaitType.Medium);
		mobileActions.click(_blendedTicket);
		mobileWaits.waitForVisibilityOfMobileElementLocated(_products, WaitType.Medium);
		mobileActions.click(_products);
/*		mobileWaits.waitForVisibilityOfMobileElementLocated(_addProduct, WaitType.Medium);
		mobileActions.click(_addProduct);
		mobileWaits.waitForVisibilityOfMobileElementLocated(_productText, WaitType.Medium);
		mobileActions.sendKeys(_productText, "urea");
		mobileWaits.waitForVisibilityOfMobileElementLocated(_addedProduct, WaitType.Medium);
		mobileActions.click(_addedProduct);
		mobileWaits.waitForVisibilityOfMobileElementLocated(_moveBackward, WaitType.Medium);
		mobileActions.click(_moveBackward);
*/		mobileWaits.waitForVisibilityOfMobileElementLocated(_conditions, WaitType.Medium);
		mobileActions.click(_conditions);
		mobileWaits.waitForVisibilityOfMobileElementLocated(_fieldCondition, WaitType.Medium);
		mobileActions.sendKeys(_fieldCondition,"paddock");
		mobileWaits.waitForVisibilityOfMobileElementLocated(_soilMoisture, WaitType.Medium);
		mobileActions.sendKeys(_soilMoisture,"Gravitational Water");
		mobileWaits.waitForVisibilityOfMobileElementLocated(_soilTexture, WaitType.Medium);
		mobileActions.sendKeys(_soilTexture,"Sandy soils");

	}
}
